namespace Trojak.K8S.EdgeOperator.Models.Definitions;

public static class API
{
    public const string Group = "edgeo-perator.k8s.dvojak.cz";
    public const string ApiVersion = "v1";
}