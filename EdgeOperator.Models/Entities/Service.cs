using k8s.Models;
using KubeOps.Operator.Entities;
using KubeOps.Operator.Entities.Annotations;
using Trojak.K8S.EdgeOperator.Models.Definitions;

namespace Trojak.K8S.EdgeOperator.Models.Entities;


public class ServiceSpec
{
    /// <summary>
    /// Name of hosted service
    /// </summary>
    [Description("Name of hosted service")]
    [Required]
    public string Name { get; set; } = null!;

    /// <summary>
    /// List of TCP ports
    /// </summary>
    [Description("List of TCP ports")]
    public IList<int>? TcpPorts { get; set; }

    /// <summary>
    /// List of UDP ports
    /// </summary>
    [Description("List of UDP ports")]
    public IList<int>? UdpPorts { get; set; }
}

/// <summary>
/// Services hosted on the device
/// </summary>
[KubernetesEntity(Group = API.Group,ApiVersion = API.ApiVersion)]
public class Service : CustomKubernetesEntity<ServiceSpec>
{
   

}