using k8s.Models;
using KubeOps.Operator.Controller;
using KubeOps.Operator.Entities;
using KubeOps.Operator.Entities.Annotations;
using Trojak.K8S.EdgeOperator.Models.Definitions;

namespace Trojak.K8S.EdgeOperator.Models.Entities;

public class ConnectionSpec
{
    [Required]
    public string IpA { get; set; } = null!;
    [Required]
    [RangeMaximum(Maximum = 65535)]
    [RangeMinimum(Minimum = 1)]
    public int PortA { get; set; }

    [Required]
    public string IpB { get; set; } = null!;
    [Required]
    [RangeMaximum(Maximum = 65535)]
    [RangeMinimum(Minimum = 1)]
    public int PortB { get; set; }

}
public class ConnectionStatus
{
}
[KubernetesEntity(Group = API.Group,ApiVersion = API.ApiVersion)]
public class Connection : CustomKubernetesEntity<ConnectionSpec,ConnectionStatus>
{
}