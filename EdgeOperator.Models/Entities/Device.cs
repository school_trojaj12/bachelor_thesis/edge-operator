using k8s.Models;
using KubeOps.Operator.Entities;
using KubeOps.Operator.Entities.Annotations;
using Trojak.K8S.EdgeOperator.Models.Definitions;

namespace Trojak.K8S.EdgeOperator.Models.Entities;


public class DeviceSpec
{

    [Required] public string Network { get; set; } = null!;

    /// <summary>
    /// IP of device in the private network
    /// </summary>
    [Description("IP of device in the private network")]
    [Required]
    public string Ip { get; set; } = null!;

    /// <summary>
    /// List of services hosted on the device
    /// </summary>
    [Description("List of services hosted on the device")]
    [Required]
    public IList<Service> Services { get; set; } = null!;
}
[KubernetesEntity(Group = API.Group,ApiVersion = API.ApiVersion)]
public class Device : CustomKubernetesEntity<DeviceSpec>
{

}