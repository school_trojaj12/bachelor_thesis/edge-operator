using DotnetKubernetesClient;
using k8s;
using k8s.Models;
using KubeOps.Operator.Controller;
using KubeOps.Operator.Controller.Results;
using KubeOps.Operator.Rbac;
using Trojak.K8S.EdgeOperator.Models.Entities;

namespace Trojak.K8S.EdgeOperator.Controllers;

[EntityRbac(typeof(Service),Verbs = RbacVerb.All)]
public class ConnectionController : IResourceController<Connection>
{
    private readonly Services.k8s _k8S;
    private readonly IKubernetesClient _kubernetesClient;
    private readonly ILogger<ConnectionController> _logger;

    public ConnectionController(
        Services.k8s k8S,
        IKubernetesClient kubernetesClient,
        ILogger<ConnectionController> logger)
    {
        _k8S = k8S;
        _kubernetesClient = kubernetesClient;
        _logger = logger;
    }

    public async Task<ResourceControllerResult?> ReconcileAsync(Connection entity)
    {
        var a = await _kubernetesClient.Get<V1Deployment>("bridge","default");
        if (a is not null)
            return null;
        var depl = _k8S.CreateConnectionForServer("bridge");
        depl = _k8S.AddSocketCommand(depl,"socat","TCP-LISTEN:8888","TCP4:localhost:9090");
        _logger.LogDebug("Object deployment called bridge will be created");
        await _kubernetesClient.Create(depl);
        _logger.LogInformation("Object deployment called bridge has been created");
        return null;
    }


    public async Task DeletedAsync(Connection entity)
    {
        var a = await _kubernetesClient.Get<V1Deployment>("bridge","default");
        if (a is not null)
        {
            _logger.LogDebug("Object deployment called bridge will be deleted");
            await _kubernetesClient.Delete(a);
            _logger.LogInformation("Object deployment called bridge has been deleted");
        }
    }
}