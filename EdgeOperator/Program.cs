using k8s;
using KubeOps.Operator;
using Trojak.K8S.EdgeOperator.Configuration;

var builder = WebApplication.CreateBuilder(args);
builder.ConfigureEdgeOperatorProject();
var app = builder.Build();
app.ConfigureEdgeOperatorApp();
await app.RunOperatorAsync(args);