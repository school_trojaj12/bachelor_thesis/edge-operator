using k8s;
using k8s.Models;

namespace Trojak.K8S.EdgeOperator.Services;

public class Maniests
{
    public readonly string ConnectionDeployment = @"---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: bridge
  namespace: default
  labels:
    app: bridge
spec:
  selector:
    matchLabels:
      app: bridge
  replicas: 1
  template:
    metadata:
      labels:
        app: bridge
    spec:
      hostNetwork: true
      containers: []
    restartPolicy: Always
";

    public readonly string Container = @"---
name: bridge
image: alpine/socat
command: []
";
}
public class k8s
{
    private readonly Maniests _maniests;

    public k8s(Maniests maniests)
    {
        _maniests = maniests;
    }

    private V1Container CreateContainer(params string[] commands)
    {
        var cont = Yaml.LoadFromString<V1Container>(_maniests.Container);
        cont.Command = commands;
        return cont;
    }

    private V1Deployment RenameDeployment(V1Deployment depl,string name)
    {
        depl.Metadata.Name = name;
        depl.Metadata.Labels["app"] = name;
        depl.Spec.Selector.MatchLabels["app"] = name;
        depl.Spec.Template.Metadata.Labels["app"] = name;
        return depl;
    }

    public V1Deployment AddSocketCommand(V1Deployment depl,params string[] commands)
    {
        depl.Spec.Template.Spec.Containers.Add(CreateContainer(commands));
        return depl;
    }

    public V1Deployment CreateConnectionForServer(string name)
    {
        var obj = Yaml.LoadAllFromString(_maniests.ConnectionDeployment)[0];
        var depl = (V1Deployment)obj;
        return RenameDeployment(depl,name);
    }
}